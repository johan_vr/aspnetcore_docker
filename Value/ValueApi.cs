using System.Threading.Tasks;
using ServiceStack.Redis;

namespace aspnetcore_docker.value {
  public class ValueApi {
    RedisManagerPool _manager;

    public ValueApi()
    {
      _manager = new RedisManagerPool("192.168.42.8:6379");
    }

    public void SetValue(string key, string value) {
      using (var client = _manager.GetClient()) {
        client.Set(key, value);
      }
    }

    public string GetValue(string key) {
      using (var client = _manager.GetClient()) {
        return client.Get<string>(key);
      }
    }
  }
}