
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace aspnetcore_docker.city {

  public class CityRepo 
  {
    public async Task<IEnumerable<City>> GetCities() {
      using (var connection = new NpgsqlConnection("Host=192.168.42.11;username=postgres;password=ubu.1900;database=dvdrental")) 
      {
        await connection.OpenAsync();
        return await connection.QueryAsync<City>("select * from city", CommandType.TableDirect);
      }
    }
  }

}