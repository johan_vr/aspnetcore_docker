namespace aspnetcore_docker.city {
  public class City {
    public int city_id { get; set; }
    public string city { get; set; }
  }
}