﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using aspnetcore_docker.value;
using Microsoft.AspNetCore.Mvc;

namespace aspnetcore_docker.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
          var api = new ValueApi();
          var result = api.GetValue(id.ToString());
          return result;
        }

        // POST api/values
        [HttpPost("{id}")]
        public void Post(int id, [FromBody]string value)
        {
          var api = new ValueApi();
          api.SetValue(id.ToString(), value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
