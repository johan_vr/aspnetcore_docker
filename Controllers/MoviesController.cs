using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace aspnetcore_docker.Controllers {
  
  [Route("api/[controller]")]
  public class MoviesController : Controller
  {
    [HttpGet]
    public async Task<object> Get()
    {
      using(var client = new HttpClient())
      {
        var result = await client.GetStringAsync("http://192.168.42.8:8080/api/values");
        return result;
      }
    }
  }
}