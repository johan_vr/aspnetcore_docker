using System;
using System.Net.Http;
using System.Threading.Tasks;
using aspnetcore_docker.city;
using Microsoft.AspNetCore.Mvc;

namespace aspnetcore_docker.Controllers
{
  [Route("api/[controller]")]
  public class CitiesController : Controller
  {
      // GET api/cites
      [HttpGet]
      public async Task<Object> Get()
      {
        var repo = new CityRepo();
        var result = await repo.GetCities();

        return result;
      }
  }
}